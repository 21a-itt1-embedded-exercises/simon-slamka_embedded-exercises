## The differences between firmware and software

### Firmware
- technically a subclass of software
- integrated into hardware
- controls low-level functions, data collection, monitoring
An example is BIOS

### Software
- can be large-scale, but can also be miniscule (an OS vs. a hello world script)
- in most cases, software runs under an operating system (however an OS is also a piece of software)
- can communicate with firmware and provides higher-level functions
